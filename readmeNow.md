1、安装命令 
npm i axios

npm install http-proxy-middleware --save

npm i react-router-dom@5.0

npm i antd

npm install -g json-server

2、初次拉取
git pull https://gitee.com/jessica-make/react-project-0203.git master --allow-unrelated-histories

3、antd 的css引入放在app.css中，已经位于全局，不需要额外在其他组件引入

4、启动后台
json-server --watch db/db.json --port 5000

5、安装粒子效果需要指定版本 用于支持react18,还需要解决依赖冲突问题
npm i react-particles-js@2.4.0 --legacy-peer-deps

6、npm版本依赖查看
https://www.npmjs.com/

7、如果遇到 Could not resolve dependency: npm ERR! peer react@"^16.0.0" from react-particles-js@2.7.1，请升级npm
npm config set legacy-peer-deps true

8、富文本编辑器 参考文档
https://jpuri.github.io/react-draft-wysiwyg/#/docs

9、