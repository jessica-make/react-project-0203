import React from 'react'

import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'

import Login from '../views/login/Login'
import NewsSandBox from '../components/sandbox/NewsSandBox'

const TokenKey = "Authorization"

export default function IndexRouter() {
  return (
    <div>
      <BrowserRouter>
        {/* Switch 有点像后端for循环的switch,只要匹配到最准确那个，就不再匹配 */}
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/" render={() =>
            //登录访问沙箱，没有登录重定向到登录页面
            localStorage.getItem(TokenKey) ?
              <NewsSandBox /> :
              <Redirect to="/login" />
          } />
        </Switch>
      </BrowserRouter>


    </div>
  )
}
