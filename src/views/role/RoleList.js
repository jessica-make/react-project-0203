import React, { useState, useEffect } from 'react'

import { Table, Button, Modal, Tree } from 'antd';
import { DeleteOutlined, EditOutlined, ExclamationCircleFilled } from '@ant-design/icons';

import axios from 'axios';

export default function RoleList() {
  const [dataSource, setDataSource] = useState([])
  const [rightList, setRightList] = useState([])
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [currentRights, setCurrentRights] = useState([])
  const [currentId, setCurrentId] = useState(0)

  useEffect(() => {
    axios.get('/roles').then(res => {
      setDataSource(res.data)
    })
  }, [])


  useEffect(() => {
    axios.get('/rights?_embed=children').then(res => {
      setRightList(res.data)
    })
  }, [])



  const showModal = (item) => {
    setIsModalOpen(true);
    setCurrentRights(item.rights);
    setCurrentId(item.id);
  };

  const handleOk = () => {
    setIsModalOpen(false);

    setDataSource(dataSource.map(item => {
      if (item.id === currentId) {
        return {
          ...item,
          rights: currentRights
        }
      }
      return item
       }))

       //patch 更新
       axios.patch(`/roles/${currentId}`,{
          rights:currentRights
       })

  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const columns = [{
    title: 'ID',
    dataIndex: 'id',
    render: (text) => <b>{text}</b>,
  },
  {
    title: '角色名称',
    dataIndex: 'roleName',
  },
  {
    title: '操作',
    render(item) {
      return <div>
        <Button danger shape="circle"
          icon={<DeleteOutlined />} onClick={() => confirmDelete(item)} />

        <Button type="primary" shape="circle"
          icon={<EditOutlined onClick={() => showModal(item)} />} />
      </div>
    },
  },
  ];

  const { confirm } = Modal;

  function confirmDelete(item) {
    confirm({
      title: '您确定要删除吗?',
      icon: <ExclamationCircleFilled />,
      onOk() {
        deleteMethod(item)
      },
      onCancel() { },
    });
  }

  const deleteMethod = (item) => {
    //假删除
    var list = dataSource.filter(k => k.id !== item.id)
    setDataSource([...list])

    //真删除
    axios.delete(`/roles/${item.id}`)
  }

  const onCheck = (checkedKeys) => {
    setCurrentRights(checkedKeys.checked)

  }

  return (
    <div>
      <Table columns={columns} dataSource={dataSource}
        pagination={{ pageSize: 5 }}
        rowKey={(item) => item.id}
      />

      <Modal title="权限分配" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <Tree
          checkable
          checkedKeys={currentRights}
          // selectedKeys={selectedKeys}
          treeData={rightList}

          checkStrictly={true}
          onCheck={onCheck}

          fieldNames={
            {
              title: 'label'
            }
          }
        />

      </Modal>

    </div>
  )
}
