import React, { useEffect, useState, useRef } from 'react'

import { Card, Col, Row, List, Avatar, Drawer, message } from 'antd';

import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';

import axios from 'axios'

import _ from 'lodash'

import * as Echarts from 'echarts'

/**
 * 首页
 * @returns 
 */
export default function Home() {
  const [viewList, setViewList] = useState([])
  const [starList, setStarList] = useState([])
  const [allList, setAllList] = useState([])

  const [open, setOpen] = useState(false);

  const [barChart, setBarChart] = useState(null)
  const [pieChart, setpieChart] = useState(null)

  const barRef = useRef()
  const pieRef = useRef()

  useEffect(() => {
    axios.get(`/news?publishState=2&_expand=category&_sort=view&_order=desc&_limit=6`)
      .then(res => {
        setViewList(res.data)
      })
  }, [])

  useEffect(() => {
    axios.get(`/news?publishState=2&_expand=category&_sort=star&_order=desc&_limit=6`)
      .then(res => {
        setStarList(res.data)
      })
  }, [])

  useEffect(() => {
    axios.get('/news?publishState=2&_expand=category').then(res => {
      //需要对返回的数据分组
      renderBarView(_.groupBy(res.data, item => item.category.label))

      setAllList(res.data)
    })

    //渲染Echarts
    const renderBarView = (obj) => {
      // 基于准备好的dom，初始化echarts实例
      var myChart;
      if (!barChart) {
        myChart = Echarts.init(barRef.current)
        setBarChart(myChart)
      } else {
        myChart = barChart
      }

      // 指定图表的配置项和数据
      var option = {
        title: {
          text: '新闻分类图示'
        },
        tooltip: {},
        legend: {
          data: ['数量']
        },
        xAxis: {
          data: Object.keys(obj),
          axisLabel: {
            rotate: "45",
            interval: 0
          }
        },
        yAxis: {
          minInterval: 1
        },
        series: [
          {
            name: '数量',
            type: 'bar',
            //Object.values(obj) 是一个Array,数组，我们只需要长度
            data: Object.values(obj).map(item => item.length)
          }
        ]
      };

      // 使用刚指定的配置项和数据显示图表。
      myChart.setOption(option);

      //监听windows 窗口往左移动的大小
      window.onresize = () => {
        myChart.resize()
      }

    }

    //销毁 需要关闭 resize对象
    return () => {
      window.onresize = null
    }

  }, [barChart])


  const renderPieView = (obj) => {
    var myChart;
    if (!pieChart) {
      myChart = Echarts.init(pieRef.current)
      setpieChart(myChart)
    } else {
      myChart = pieChart
    }

    //数据处理工作
    var currentList = allList.filter(item => item.author === username)
    var groupObj=_.groupBy(currentList, item => item.category.label)

    var list=[]
    for(var i in groupObj){
      list.push({
         name:i,
         value:groupObj[i].length
      })
    }

    var option = {
      title: {
        text: '当前新闻分类图示',
        subtext: '',
        left: 'center'
      },
      tooltip: {
        trigger: 'item'
      },
      legend: {
        orient: 'vertical',
        left: 'left'
      },
      series: [
        {
          name: '发布数量',
          type: 'pie',
          radius: '50%',
          data:list,
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    };

    option && myChart.setOption(option);
  }


  const { Meta } = Card;

  const { username, region, role: { roleName } } = JSON.parse(localStorage.getItem("Authorization"))

  const onClose = () => {
    setOpen(false);
  };


  return (
    <div>
      <Row gutter={16}>
        <Col span={8}>
          <Card title="用户最常浏览的新闻" bordered={true}>
            <List
              size="large"
              dataSource={viewList}
              renderItem={(item) => <List.Item>
                <a href={`/news-manage/preview/${item.id}`}>{item.title}</a>
              </List.Item>}
            />
          </Card>
        </Col>
        <Col span={8}>
          <Card title="用户点赞最多的新闻" bordered={true}>
            <List
              size="large"
              dataSource={starList}
              renderItem={(item) => <List.Item>
                <a href={`/news-manage/preview/${item.id}`}>{item.title}</a>
              </List.Item>}
            />
          </Card>
        </Col>
        <Col span={8} >
          <Card 
            cover={
              <img
                alt="example"
                src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
              />
            }
            actions={[
              <SettingOutlined key="setting" onClick={() => {
                setOpen(true)

                //让这个先不要渲染，等把open的值挂上去在渲染
                setTimeout(() => {
                  renderPieView()
                }, 0);
              }}
              />,
              <EditOutlined key="edit" onClick={()=>{message.info("该功能尚未开发,需要加Q!")}}/>,
              <EllipsisOutlined key="ellipsis" onClick={()=>{message.info("该功能尚未开发,需要加Q!")}}/>,
            ]}
          >
            <Meta
              avatar={<Avatar src="https://joesch.moe/api/v1/random" />}
              title={username}
              description={
                <div>
                  <b>{region ? region : "全球"}</b>
                  <span style={{ paddingLeft: '20px' }}>
                    {roleName}
                  </span>
                </div>
              }
            />
          </Card>
        </Col>
      </Row>


      <Drawer title="个人新闻分类"
        width="500px"
        placement="right" onClose={onClose} open={open}>
        <div ref={pieRef} style={{
          width: "100%",
          height: "400px",
          marginTop: "30px"
        }}></div>
      </Drawer>

      <div ref={barRef} style={{
        width: "100%",
        height: "400px",
        marginTop: "30px"
      }}></div>

    </div>

  )
}
