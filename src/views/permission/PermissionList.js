import React, { useEffect, useState } from 'react'

import { DeleteOutlined, EditOutlined, ExclamationCircleFilled } from '@ant-design/icons';
import { Table, Tag, Button, Alert, Modal, Popover, Switch, message } from 'antd'

import Marquee from 'react-fast-marquee';

import axios from 'axios';

import pubsub from 'pubsub-js'

export default function PermissionList() {
  const [dataSource, setDataSource] = useState([])

  useEffect(() => {
    axios.get("/rights?_embed=children").then(res => {
      res.data.map((obj) => {
        if (obj.children?.length <= 0) {
          obj.children = undefined
        }
        return obj
      })

      setDataSource(res.data)
    })

  }, [])


  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      render(id) {
        return <b>{id}</b>
      },
    },
    {
      title: '权限名称',
      dataIndex: 'label',
    },
    {
      title: '权限路径',
      dataIndex: 'key',
      render(key) {
        return <Tag color='orange'>{key}</Tag>
      },
    },
    {
      title: '操作',
      render(item) {
        return <div>
          <Button danger shape="circle"
            icon={<DeleteOutlined />} onClick={() => confirmDelete(item)} />


          {/* js 语法复习  checked={item.pagepermisson}  disabled={!item.pagepermisson}
           js 中 1是true,0是false
           所以，当item.pagepermisson为1,是true,checked会打开 
           !item.pagepermisson为false，所以disabled为false 不会启用禁用
         */}
          <Popover content={
            <div style={{ textAlign: 'center' }}>
              <Switch checked={item.pagepermisson} onChange={() => handleSwitch(item)}>打开</Switch>
            </div>
          } title="页面配置项" trigger="click">
            <Button type="primary" shape="circle"
              icon={<EditOutlined />} disabled={!item.pagepermisson} />
          </Popover>

        </div>
      },
    },
  ];

  const handleSwitch = (item) => {
    item.pagepermisson = item.pagepermisson === 1 ? 0 : 1

    //如果是第一层，改最外面的接口
    if (item.grade === 1) {
      //patch 补丁请求
      axios.patch(`/rights/${item.id}`, {
        pagepermisson: item.pagepermisson
      })

    } else {
      axios.patch(`/children/${item.id}`, {
        pagepermisson: item.pagepermisson
      })
    }

    setDataSource([...dataSource])

    //更新菜单侧边栏和

    //首页权限还是接收不到
    pubsub.publish("isUpdate",!item.pagepermisson)

  }

  const { confirm } = Modal;

  function confirmDelete(item) {
    confirm({
      title: '您确定要删除吗?',
      icon: <ExclamationCircleFilled />,
      onOk() {
        deleteMethod(item)
      },
      onCancel() { },
    });
  }

  const deleteMethod = (item) => {
    //注意，二级权限删不掉是因为，/rights 只展示一级路由

    if (item.grade === 1) {
      //表格数据假删除
      setDataSource(dataSource.filter(k => k.id !== item.id))

      //表格数据真删除
      // axios.delete(`/rights/${item.id}`)
     
      var shouldUpdate=item.pagepermisson
      // console.log(shouldUpdate===1);

      //需要打开真删除，才可以实现效果
      pubsub.publish("isUpdate",shouldUpdate===1)

      message.info("需要打开权限列表的真删除，才可以实现实时刷新左侧菜单")
      return
    }

    //多级数据假删除，先找到最上面那一级数组
    var list = dataSource.filter(k => k.id === item.rightId)

    //这个数组只有一条数据，有自己的属性和children数组
    list[0].children = list[0].children.filter(k => k.id !== item.id)
    setDataSource([...dataSource])

    //多级数据真删除
    axios.delete(`/children/${item.id}`)
  }

  return (
    <div>
      <Alert
        banner
        message={
          <Marquee pauseOnHover gradient={false}>
            React web管理系统,欢迎私人定制,联系邮箱 1164185650@qq.com
          </Marquee>
        }
      />
      <Table dataSource={dataSource} columns={columns} pagination={{ pageSize: 5 }} />
    </div>
  )
}
