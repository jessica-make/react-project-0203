import React from 'react'

import { LockOutlined, UserOutlined } from '@ant-design/icons';

import { Button, Form, Input, message } from 'antd';

import Particles from 'react-particles-js';

import './Login.css'

import axios from 'axios';

export default function Login(props) {

  /** 需要实现 先弹出提示，然后跳转首页 */
  // const LoginToHome= async (res)=>{
  //   success("登录成功")
  //   toHome(res)
  // }

  // const toHome=(res)=>{
  //   localStorage.setItem("token",res.data[0])
  //   props.history.replace("/")
  // }

  const onFinish = async (values) => {
    axios.get(`/users?username=${values.username}&password=${values.password}&roleState=true&_expand=role`).then(res => {
      if (res.data.length === 0) {
        message.error("密码错误,请重新输入")
      } else {
        //async await 没有整明白 暂时用定时器做了
        //  LoginToHome(res)

        message.success("登录成功")
        setTimeout(() => {
          localStorage.setItem("Authorization", JSON.stringify(res.data[0]))
          props.history.replace("/")
        }, 500);
      }

    })
  };

  return (
    <div style={{ background: 'rgb(35,39,65)', height: "100vh", overflow: 'hidden' }}>

      <Particles
        height={document.documentElement.clientHeight}
        params={{
          particles: {
            number: {
              value: 50
            }, size: {
              value: 6
            }
          }, interactivity: {
            events: {
              onhover: {
                enable: true, mode: 'repulse'
              }
            }
          }
        }}
      />

      <div className='formContainer'>
        <div className='loginTitle'>React web管理系统</div>
        <Form
          name="normal_login"
          className="login-form"
          onFinish={onFinish}
        >
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: '用户名不能为空!',
              },
            ]}
          >
            <Input prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="请输入您的用户名" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: '密码不能为空!',
              },
            ]}
          >
            <Input prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="请输入您的密码"
            />
          </Form.Item>

          <Form.Item style={{ textAlign: 'center', marginTop: '8vh' }}>
            <Button style={{ width: '20vh' }}
              type="primary" htmlType="submit" className="login-form-button">
              登录
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}
