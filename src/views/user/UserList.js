import React, { useState, useEffect } from 'react'

import { Table, Button, Modal, Switch, Form } from 'antd';
import { DeleteOutlined, EditOutlined, ExclamationCircleFilled } from '@ant-design/icons';

import axios from 'axios';
import './UserList.css'

import UserForm from '../../components/user-manage/UserForm';

import pubsub from 'pubsub-js';

export default function UserList() {
  const [dataSource, setDataSource] = useState([])
  const [updateRoleState, setUpdateRoleState] = useState(false)
  const [open, setOpen] = useState(false);
  const [modelTitle, setModelTitle] = useState("");

  //用于新增用户 表单里面的展示数据
  const [roleList, setRoleList] = useState([])
  const [regionList, setRegionList] = useState([])

  //用于修改用户,获取用户id
  const [itemList, setItemList] = useState(null)

  const [form] = Form.useForm();

  const { roleId, region,username,role:{roleName} } = JSON.parse(localStorage.getItem("Authorization"))

  useEffect(() => {

    const roleObj = {
      "1": "superadmin",
      "2": "admin",
      "3": "editor"
    }

    axios.get('/users?_expand=role').then(res => {
      var list = res.data

      //返回列表数据前做一次过滤，如果是超级管理员，返回全部数据
      //否则需要做一次过滤
      setDataSource(roleObj[roleId]==="superadmin"?list:[
        //先拿到自己的数据
            ...list.filter(item=>item.username===username),
            //再拿到和自己一个区域的数据，[]会做拼接
            ...list.filter(item=>item.region===region &&
              roleObj[item.roleId] === "editor"
              )
         ]
      )
    })
  }, [updateRoleState,region,roleId,username])

  //用于新增表单里面展示
  useEffect(() => {
    axios.get('/regions').then(res => {
      setRegionList(res.data)
    })
  }, [])

  //用于新增表单里面展示
  useEffect(() => {
    axios.get('/roles').then(res => {
      setRoleList(res.data)
    })
  }, [])


  const columns = [{
    title: '区域',
    dataIndex: 'region',
    filters: [
      ...regionList.map(item => ({
        text: item.label,
        value: item.value
      })

      ),
      {
        text: "全球",
        value: "全球"
      }

    ],
    onFilter: (value, item) => {
      if (value === '全球') {
        return item.region === ""
      }
      return item.region === value
    },
    render: (text) => <b>{text === '' ? "全球" : text}</b>,
  },
  {
    title: '角色名称',
    dataIndex: 'role',
    render: (role) => <span>{role?.roleName}</span>
  },
  {
    title: '用户名',
    dataIndex: 'username',
  },
  {
    title: '用户状态',
    dataIndex: 'roleState',
    render: (roleState, item) => {
      return <Switch checked={roleState} disabled={item.default}
        onClick={() => handleChecked(item)}
      ></Switch>
    }
  },
  {
    title: '操作',
    render(item) {
      return <div>
        <Button danger shape="circle"
          icon={<DeleteOutlined />} onClick={() => confirmDelete(item)}
          disabled={item.default}
        />

        <Button type="primary" shape="circle"
          icon={<EditOutlined />} disabled={item.default}
          onClick={() => openUpdateModal(item)}
        />
      </div>
    },
  },
  ];

  const { confirm } = Modal;

  function confirmDelete(item) {
    confirm({
      title: '您确定要删除吗?',
      icon: <ExclamationCircleFilled />,
      onOk() {
        deleteMethod(item)
      },
      onCancel() { },
    });
  }

  const deleteMethod = (item) => {
    //假删除
    setDataSource(dataSource.filter(k => k.id !== item.id))

    //真删除
    axios.delete(`/users/${item.id}`)
  }

  //修改 设置数据到表单，然后打开模态框，如果是超级管理员，角色不可以选，然后
  const openUpdateModal = (item) => {
    setOpen(true)
    setModelTitle("修改用户")
    setItemList(item)

    if (item.roleId === 1) {
      //禁用 
      pubsub.publish("shouldIsDisabled", true)
    } else {
      //取消禁用
      pubsub.publish("shouldIsDisabled", false)
    }

    form.setFieldsValue({ ...item })
  }

  const handleChecked = (item) => {
    setUpdateRoleState(!updateRoleState)
    axios.patch(`/users/${item.id}`, {
      roleState: !item.roleState
    })
  }

  //提交表单数据到后台,首先判读是新增还是修改
  //如果是添加用户，数据插入到后台 post
  //如果是修改用户，数据打补丁到后台 patch
  const onCreateOrUpdate = (values) => {
    setOpen(false);
    if (modelTitle === "添加用户") {
      //新增需要重置表单
      form.resetFields();
      //保存到dataSource
      axios.post('/users', {
        ...values,
        "roleState": true,
        "default": values.roleId === 1 ? true : false
      }).then(res => {
        setDataSource([...dataSource, res.data])
        setUpdateRoleState(!updateRoleState)
      })
    } else {
      axios.patch(`/users/${itemList.id}`, {
        ...values
      }).then(_ => {
        //更新不需要设置 setDataSource，因为已经有这一条数据了
        setUpdateRoleState(!updateRoleState)
      })

    }

  };

  const onCancel = () => {
    form.resetFields();
    setOpen(false)
  }

  return (
    <>
      <Button type="primary" onClick={() => { setOpen(true); setModelTitle("添加用户") }}>添加用户</Button>
      <Table columns={columns} dataSource={dataSource}
        pagination={{ pageSize: 5 }}
        rowKey={(item) => item.id}
      />

      <Modal
        open={open}
        title={modelTitle}
        okText="确定"
        cancelText="取消"
        onCancel={onCancel}
        onOk={() => {
          form.validateFields().then((values) => {
            //新增或者更新用户
            onCreateOrUpdate(values);
          }).catch((info) => {
            console.log('表单提交失败:', info);
          });
        }}
      >
        <UserForm form={form} regionList={regionList} modelTitle={modelTitle} roleName={roleName}
          roleList={roleList} region={region}
        ></UserForm>

      </Modal>
    </>
  )
}
