import axios from "axios";

import {store} from "../../redux/store";

axios.defaults.baseURL="http://localhost:5000"

// axios.defaults.headers

/**
 * axios 请求前拦截
 */
axios.interceptors.request.use(function (config) {
    //数据请求之前 显示 loading
    store.dispatch({
       type:'change_Loading',
       payload:true
    })
    return config;
  }, function (error) {
    return Promise.reject(error);
  });


/**
 * axios 请求后拦截
 */
axios.interceptors.response.use(function (response) {
    //数据请求之后 隐藏 loading

    store.dispatch({
      type:'change_Loading',
      payload:false
   })

    return response;
  }, function (error) {
    //数据请求之后 隐藏 loading

    store.dispatch({
      type:'change_Loading',
      payload:false
   })

    return Promise.reject(error);
  });