export function success (messageApi,content) {
  messageApi.open({
    type: 'success',
    content: content
  });
};

export function error(messageApi,content) {

  messageApi.open({
    type: 'error',
    content: content
  });
};

