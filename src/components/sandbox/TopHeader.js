import React from 'react'

import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  DownOutlined,
  UserOutlined 
} from '@ant-design/icons';

import { Layout,Dropdown,Space,Avatar } from 'antd';

import { withRouter } from 'react-router-dom'

import {connect} from 'react-redux'

//右边顶部栏
const TopHeader=(props)=> {

  //redux 这里分发状态
  const handleCollapsed=()=>{
    props.changeCollapsed()
  }

  const { Header } = Layout;

  const {role:{roleName},username} =JSON.parse(localStorage.getItem("Authorization"))

  const items = [
    {
      key: '1',
      label: (
        <a target="_blank" rel="noopener noreferrer">
          {roleName}
        </a>
      ),
    },
    {
      key: '2',
      label: (
        <a target="_blank" rel="noopener noreferrer"
          onClick={()=>{
            redirectToLogin()
          }}
         >
          退出
        </a>
      ),
    }
    
  ];

  

  //退出登录
  function redirectToLogin(){
  localStorage.removeItem("Authorization")
    props.history.replace("/login")
  }

  return (
    // padding 内容到Header边缘的距离  padding: '0 16px' 上下不动，距离左边16px
    //margin  Header组件与其他组件的距离
    <Header style={{ padding: '0 16px', background: props.colorBgContainer }}>

      {React.createElement(props.isCollapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
        className: 'trigger',
        onClick: () => handleCollapsed(),
      })}

      {/* {
              props.collapsed?<MenuUnfoldOutlined/>:<MenuFoldOutlined/>
          } */}


      <div style={{ float: "right" }}>
        {/* div里面放个span 而不直接写div+内容，原因是
            直接写有可能内容很长，导致换行，span内容过长不会换行
        */}
        <span onClick={()=>{
        }}>欢迎<span style={{color:'#1890ff'}}>{username}</span>回来</span>

     <Dropdown
          menu={{
            items,
          }}>
          <a onClick={(e) => e.preventDefault()} href='#'>
            <Space>
            <Avatar size="large" icon={<UserOutlined />} />
              <DownOutlined />
            </Space>
          </a>
        </Dropdown> 

      </div>

    </Header>
  )
}


//获取折叠的初始属性
const mapStateToProps=({CollapsedReducer:{isCollapsed}})=>{
    return {
      isCollapsed
    }
}

//整个页面加载的时候，会把mapDispatchToProps 传给redux, 一旦点击了 handleCollapsed
//changeCollapsed() 这个Action 会发给 
//修改 触发changeCollapsed方法
const mapDispatchToProps={
  changeCollapsed(){
    return {
      type:'change_collapsed',
      // payload:''
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(withRouter(TopHeader))