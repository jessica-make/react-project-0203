import React, { useEffect, useState } from 'react'

import { Switch, Route, Redirect } from 'react-router-dom'

import Home from '../../views/home/Home'
import UserList from '../../views/user/UserList'
import RoleList from '../../views/role/RoleList'
import PermissionList from '../../views/permission/PermissionList'
import NoPermission from '../../views/nopermission/NoPermission'

import NewsAdd from '../news-manage/NewsAdd'
import NewsDraft from '../news-manage/NewsDraft'
import NewsCategory from '../news-manage/NewsCategory'
import NewsPreview from '../news-manage/NewsPreview'
import NewsUpdate from '../news-manage/NewsUpdate'

import Audit from '../audit-manage/Audit'
import AuditList from '../audit-manage/AuditList'

import Unpublished from '../publish-manage/Unpublished'
import Published from '../publish-manage/Published'
import Sunset from '../publish-manage/Sunset'
import axios from 'axios'

import { Watermark,Spin } from 'antd';

import {connect} from 'react-redux'

const LocalRouterMap = {
    "/home": Home,
    "/user-manage/list": UserList,
    "/right-manage/role/list": RoleList,
    "/right-manage/right/list": PermissionList,
    "/news-manage/add": NewsAdd,
    "/news-manage/draft": NewsDraft,
    "/news-manage/category": NewsCategory,
    "/news-manage/preview/:id": NewsPreview,
    "/news-manage/update/:id":NewsUpdate,
    "/audit-manage/audit": Audit,
    "/audit-manage/list": AuditList,
    "/publish-manage/unpublished": Unpublished,
    "/publish-manage/published": Published,
    "/publish-manage/sunset": Sunset
}


 function NewsRouter(props) {
    const [backRouteList, setbackRouteList] = useState([])

    useEffect(() => {
        Promise.all(([
            axios.get("/rights"),
            axios.get("/children")
        ])).then(res => {
            setbackRouteList([...res[0].data, ...res[1].data])
        })
    }, [])

    const { role: { rights } } = JSON.parse(localStorage.getItem("Authorization"))

    //首先本地有这个组件,并且页面有展示权
    const checkRoute = (item) => {
        return LocalRouterMap[item.key] && (item.pagepermisson || item.routepermisson)
    }

    const checkUserPermission = (item) => {
        return rights.includes(item.key)
    }

    return (
        <Spin size="large" spinning={props.isLoading}>
        <Watermark content="React Web管理系统">
            <Switch>
                {
                    backRouteList.map(item => {
                        if (checkRoute(item) && checkUserPermission(item)) {
                            return <Route path={item.key} key={item.key}
                                component={LocalRouterMap[item.key]} exact />
                        }
                        //底下去判断
                        return null
                    }

                    )
                }

                {/* 如果直接输入 http://localhost:3000/ 重定向到
                http://localhost:3000/home 首页,这里我们想起了常用的网站
                为什么我们输入一个域名，会来到一个网站的首页
                */}
                <Redirect from="/" to="/home" exact />

                {/* 解决路由数据还没有返回，就403 无此权限 */}
                {
                    backRouteList.length > 0 && <Route path="*" component={NoPermission} />
                }
            </Switch>
        </Watermark>
       </Spin>
    )
}

//获取loading的初始属性
const mapStateToProps=({LoadingReducer:{isLoading}})=>{
    return {
        isLoading
    }
  }

export default connect(mapStateToProps)(NewsRouter)
