import React, {useEffect} from 'react'

import SideMenu from '../sidemenu/SideMenu'
import TopHeader from './TopHeader'

import NewsRouter from './NewsRouter'

import NProgress from 'nprogress'

import 'nprogress/nprogress.css'

import './NewsSandBox.css'

import { Layout ,theme  } from 'antd';

const {Content } = Layout;

export default function NewsSandBox() {


  //路由进度条
  NProgress.start()

  useEffect(() => {
    NProgress.done();
  })
  
  const {
    token: { colorBgContainer },
  } = theme.useToken();

    return (
      <Layout>
        <SideMenu ></SideMenu>
        <Layout className="site-layout">
          <TopHeader colorBgContainer={colorBgContainer}
          ></TopHeader>

        <Content
           style={{
            margin: '24px 16px',
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
            overflow:'auto' //加上这个 不会和侧边栏一起滚动
          }}>

         <NewsRouter></NewsRouter>
         
        </Content>

        </Layout>

      </Layout>
    )
  
}
