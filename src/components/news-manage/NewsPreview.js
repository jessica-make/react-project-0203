import React, { useState, useEffect } from 'react'

import { Descriptions, Button } from 'antd';
import { ArrowLeftOutlined } from '@ant-design/icons';

import moment from 'moment'

import axios from 'axios';

export default function NewsPreview(props) {
  const [newsInfo, setNewsInfo] = useState(null)

  const id = props.match.params.id

  const auditList = ["未审核", "审核中", "已通过", "未通过"]
  const publishList = ["未发布", "待发布", "已上线", "已下线"]

  const colorList = ["black", "orange", "green", "red"]

  useEffect(() => {
    axios.get(`/news/${id}?_expand=category&_expand=role`).then(res => {
      setNewsInfo(res.data)
    }
    )
  }, [id])

  const backToNewsDraft = () => {
    //这里不能用push 因为预览界面 在草稿箱和审核列表 界面都用到过
    // props.history.push('/news-manage/draft')

    //用go回退
    props.history.go(-1)
  }

  return (
    <div>
      {
        newsInfo &&
        <Descriptions title={<span><Button onClick={() => backToNewsDraft()} style={{ border: 'none' }} icon={<ArrowLeftOutlined />}></Button><span style={{ padding: '0 5px' }}>{newsInfo.title}</span><span style={{ color: '#ccc' }}>{newsInfo.category.label}</span></span>} column={3} bordered>
          <Descriptions.Item label="创建者">{newsInfo.author}</Descriptions.Item>
          <Descriptions.Item label="创建时间">{moment(newsInfo.createTime).format("YYYY-MM-DD HH:mm:ss")}</Descriptions.Item>

          <Descriptions.Item label="发布时间">
            {
              newsInfo.publishTime ? moment(newsInfo.publishTime).format("YYYY-MM-DD HH:mm:ss") :
                "-"
            }
          </Descriptions.Item>

          <Descriptions.Item label="区域">{newsInfo.region}</Descriptions.Item>

          <Descriptions.Item label="审核状态">
            <span style={{ color: colorList[newsInfo.auditState] }}>{auditList[newsInfo.auditState]}</span>
          </Descriptions.Item>

          <Descriptions.Item label="发布状态">
            <span style={{ color: colorList[newsInfo.publishState] }}>{publishList[newsInfo.publishState]}</span>
          </Descriptions.Item>

          <Descriptions.Item label="访问数量">
            {newsInfo.view}
          </Descriptions.Item>
          <Descriptions.Item label="点赞数量">{newsInfo.star}</Descriptions.Item>
          <Descriptions.Item label="评论数量">0</Descriptions.Item>

          <Descriptions.Item label="新闻内容">
            <div dangerouslySetInnerHTML={
              {
                __html: newsInfo?.content
              }
            }>
            </div>
          </Descriptions.Item>
        </Descriptions>
      }



    </div>
  )
}
