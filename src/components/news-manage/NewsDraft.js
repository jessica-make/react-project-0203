import React, { useState, useEffect } from 'react'

import { Table, Button,Modal, notification} from 'antd';
import { DeleteOutlined, EditOutlined,UploadOutlined,ExclamationCircleFilled } from '@ant-design/icons';

import axios from 'axios';

export default function NewsDraft(props) {
  const [dataSource, setDataSource] = useState([])

  const { username } = JSON.parse(localStorage.getItem("Authorization"))

  useEffect(() => {
    axios.get(`/news?author=${username}&auditState=0&_expand=category`)
      .then(res => {
        setDataSource(res.data)
      })
  }, [username])


  const columns = [{
    title: 'ID',
    dataIndex: 'id',
    render: (text) => <b>{text}</b>,
  },
  {
    title: '新闻标题',
    dataIndex: 'title',
    render: (tit,item) => <a href={`/news-manage/preview/${item.id}`}>{tit}</a>
  },

  {
    title: '作者',
    dataIndex: 'author',
  },

  {
    title: '分类',
    dataIndex: 'category',
    render: (category) => {
      return category ===undefined?"":category.label
    }
  },
  {
    title: '操作',
    render(item) {
      return <div>
        <Button danger shape="circle"
          icon={<DeleteOutlined />} onClick={() => confirmDelete(item)} />

        <Button  shape="circle"
          icon={<EditOutlined />} onClick={()=>
           props.history.push(`/news-manage/update/${item.id}`)
          }/>

        <Button type="primary" shape="circle"
          icon={<UploadOutlined />} onClick={()=>handleCheck(item.id)}/>
      </div>
    },
  },
  ];


  const { confirm } = Modal;

  function confirmDelete(item) {
    confirm({
      title: '您确定要删除吗?',
      icon: <ExclamationCircleFilled />,
      onOk() {
        deleteMethod(item)
      },
      onCancel() { },
    });
  }

  const deleteMethod = (item) => {
    //假删除
    var list = dataSource.filter(k => k.id !== item.id)
    setDataSource([...list])

    //真删除
    axios.delete(`/news/${item.id}`)
  }

  const handleCheck=(id)=>{
    const { confirm } = Modal;
      confirm({
        title: '您确定要提交审核吗?',
        icon: <ExclamationCircleFilled />,
        onOk() {
          commitDraft(id)
        },
        onCancel() { },
      });
  }

  const commitDraft=(id)=>{
    axios.patch(`/news/${id}`,{
      auditState:1
     }).then(res=>
      setTimeout(() => {
        props.history.push('/audit-manage/list')
      }, 500),
      notification.info({
        message: "通知",
        description:`您可以到审核列表中查看您的新闻`,
        placement:"bottomRight"
      })
     )
  }

  return (
    <div>
      <Table columns={columns} dataSource={dataSource}
        pagination={{ pageSize: 5 }}
        rowKey={(item) => item.id}
      />
    </div>
  )
}
