import React, { useEffect, useState,useRef,useContext } from 'react'

import { DeleteOutlined, ExclamationCircleFilled } from '@ant-design/icons';
import { Table, Button, Modal,Form, Input, message } from 'antd'

import axios from 'axios';

export default function NewsCategory() {
  const [dataSource, setDataSource] = useState([])

  const EditableContext = React.createContext(null);

  useEffect(() => {
    axios.get("/categories").then(res => {
      setDataSource(res.data)
    })
  }, [])


  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      render(id) {
        return <b>{id}</b>
      },
    },
    {
      title: '栏目名称',
      dataIndex: 'label',
      onCell: (record) => ({
        record,
        editable: true,
        dataIndex: 'label',
        title: '栏目名称',
        handleSave,
      }),
    },
    {
      title: '操作',
      render(item) {
        return <div>
          <Button danger shape="circle"
            icon={<DeleteOutlined />} onClick={() => confirmDelete(item)} />

        </div>
      },
    },
  ];

  const handleSave=(record)=>{
    setDataSource(dataSource.map(item=>{
      if(item.id === record.id){
           return {
             id:item.id,
             label:record.label,
             value:record.label
           }
      }
      //不是这一项 返回默认的
      return item
    }))

    //这里没有写错，页面上只能显示一个label 后台需要修改2个地方
       axios.patch(`/categories/${record.id}`,{
         label:record.label,
         value:record.label
       })
  }

  const { confirm } = Modal;

  function confirmDelete(item) {
    confirm({
      title: '您确定要删除吗?',
      icon: <ExclamationCircleFilled />,
      onOk() {
        deleteMethod(item)
      },
      onCancel() { },
    });
  }

  const deleteMethod = (item) => {
      setDataSource(dataSource.filter(k => k.id !== item.id))
      // axios.delete(`/categories/${item.id}`)
      message.info("新闻分类数量较少，后台并没有实际删除")
  }


  const EditableRow = ({ index, ...props }) => {
    const [form] = Form.useForm();
    return (
      <Form form={form} component={false}>
        <EditableContext.Provider value={form}>
          <tr {...props} />
        </EditableContext.Provider>
      </Form>
    );
  };
  const EditableCell = ({
    title,
    editable,
    children,
    dataIndex,
    record,
    handleSave,
    ...restProps
  }) => {
    const [editing, setEditing] = useState(false);
    const inputRef = useRef(null);
    const form = useContext(EditableContext);
    useEffect(() => {
      if (editing) {
        inputRef.current.focus();
      }
    }, [editing]);
    const toggleEdit = () => {
      setEditing(!editing);
      form.setFieldsValue({
        [dataIndex]: record[dataIndex],
      });
    };
    const save = async () => {
      try {
        const values = await form.validateFields();
        toggleEdit();
        handleSave({
          ...record,
          ...values,
        });
      } catch (errInfo) {
        console.log('Save failed:', errInfo);
      }
    };
    let childNode = children;
    if (editable) {
      childNode = editing ? (
        <Form.Item
          style={{
            margin: 0,
          }}
          name={dataIndex}
          rules={[
            {
              required: true,
              message: `${title} is required.`,
            },
          ]}
        >
          <Input ref={inputRef} onPressEnter={save} onBlur={save} />
        </Form.Item>
      ) : (
        <div
          className="editable-cell-value-wrap"
          style={{
            paddingRight: 24,
          }}
          onClick={toggleEdit}
        >
          {children}
        </div>
      );
    }
    return <td {...restProps}>{childNode}</td>;
  };
  
  
  return (
    <div>
      <Table dataSource={dataSource} columns={columns} 
      pagination={{ pageSize: 5 }} 
       rowKey={item=>item.id}

       components={
        {
          body: {
            row: EditableRow,
            cell: EditableCell,
          }
        }
       }
      />
    </div>
  )
}
