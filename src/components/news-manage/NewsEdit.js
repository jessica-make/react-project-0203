import React, { useState, useEffect } from 'react'

import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { EditorState, convertToRaw, ContentState } from 'draft-js';

import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

export default function NewsEdit(props) {

    const [editorState, setEditorState] = useState("")

    useEffect(() => {
        //html 转成 富文本draft
        const html = props.content
        //撰写新闻初始没有内容
        if(html === undefined) return
        const contentBlock = htmlToDraft(html)
        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            setEditorState(editorState)
        }

    }, [props.content])

    return (
        <div>
            <Editor
                editorState={editorState}
                toolbarClassName="toolbarClassName"
                wrapperClassName="wrapperClassName"
                editorClassName="editorClassName"

                //修改赋值
                onEditorStateChange={(editData) =>
                    setEditorState(editData)
                }

                //失去焦点传递，保证数据写完
                onBlur={() => {
                    props.getContent(draftToHtml(convertToRaw(
                        editorState.getCurrentContent())))
                }}
            />
        </div>
    )
}
