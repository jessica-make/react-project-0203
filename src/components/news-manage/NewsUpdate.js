import React, { useState, useEffect, useRef } from 'react'

import { Button, Steps, Form, Input, Select, message, notification } from 'antd';

import style from './NewsAdd.module.css'

import { ArrowLeftOutlined } from '@ant-design/icons';

import axios from 'axios'

import NewsEdit from './NewsEdit'

export default function NewsUpdate(props) {

    const [currentId, setCurrentId] = useState(0)
    const [categoryList, setCategoryList] = useState([])
    const NewsForm = useRef([])

    const [formInfo, setFormInfo] = useState({})
    const [formContent, setFormContent] = useState("")

    const { Option } = Select


    const id = props.match.params.id

    useEffect(() => {
        axios.get("/categories").then(res => {
            setCategoryList(res.data)
        })
    }, [])

    useEffect(() => {
        axios.get(`/news/${id}?_expand=category&_expand=role`).then(res => {
            let {title,categoryId,content}=res.data
            NewsForm.current.setFieldsValue({
                title,
                categoryId
            })

            setFormContent(content)
         }
        )
      }, [id])

    const backToNewsDraft = () => {
        //两种方式 props.history.push('/news-manage/draft') 
        props.history.goBack()
      }

    const itemList = [
        {
            title: '基本信息',
            description: '新闻标题,新闻分类',
        },
        {
            title: '新闻内容',
            description: '新闻主体内容',
        },
        {
            title: '新闻提交',
            description: '保存草稿或提交审核',
        }
    ]


    const handleNext = () => {
        if (currentId === 0) {
            NewsForm.current.validateFields().then(res => {
                setFormInfo(res)
                setCurrentId(currentId + 1)
            }).catch(_ =>
                message.error("表单数据不能为空")
            )
        } else {
            if (formContent === "" || formContent.trim() === "<p></p>") {
                message.error("新闻内容不能为空")
                return
            } else {
                setCurrentId(currentId + 1)
            }

        }

    }

    const handlePrev = () => {
        setCurrentId(currentId - 1)
    }

    const handleSave = (auditState) => {
        axios.patch(`/news/${id}`, {
            ...formInfo,
            "content": formContent,
            "auditState": auditState
        }).then(res => {
            setTimeout(() => {
                props.history.push(auditState === 0
                    ? "/news-manage/draft" : "/audit-manage/list")
            }, 500);

            notification.info({
                message: "通知",
                description: `您可以到${auditState === 0 ? "草稿箱" : "审核列表"}中查看您的新闻`,
                placement: "bottomRight"
            });
        })
    }

    return (
        <div>
            <span style={{display:'flex'}}>
                <Button onClick={() => backToNewsDraft()}
                    style={{ border: 'none',marginTop:'-5px' }} icon={<ArrowLeftOutlined />}>
                </Button>
                <h1><b>更新新闻</b></h1>
            </span>

            <br />
            <br />

            <Steps
                current={currentId}
                items={itemList}
            />

            <div style={{ marginTop: '5vh' }}>
                <div className={currentId === 0 ? '' : style.active}>
                    <Form
                        name="basic"
                        labelCol={{
                            span: 4,
                        }}
                        wrapperCol={{
                            span: 20,
                        }}
                        style={{
                            maxWidth: 600,
                            left: 0
                        }}
                        initialValues={{
                            remember: true,
                        }}
                        autoComplete="off"

                        ref={NewsForm}
                    >
                        <Form.Item
                            label="新闻标题"
                            name="title"
                            rules={[
                                {
                                    required: true,
                                    message: '新闻标题不能为空!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="新闻分类"
                            name="categoryId"
                            rules={[
                                {
                                    required: true,
                                    message: '请选择新闻类别!',
                                },
                            ]}
                        >
                            <Select
                                style={{
                                    width: 502,
                                }}>

                                {
                                    categoryList.map(k =>
                                        <Option value={k.id} key={k.id}>{k.value}</Option>
                                    )
                                }
                            </Select>
                        </Form.Item>

                    </Form>
                </div>
            </div>


            <div style={{ marginTop: '5vh' }}>
                <div className={currentId === 1 ? '' : style.active}>
                    <NewsEdit getContent={(value) => {
                        setFormContent(value)
                    }} content={formContent}></NewsEdit>
                </div>
            </div>

            <div style={{ marginTop: '20vh' }}>
                {
                    currentId === 2 && <span>
                        <Button type="primary" onClick={() => handleSave(0)}>保存草稿</Button>
                        <Button danger onClick={() => handleSave(1)}>提交审核</Button>
                    </span>
                }

                {
                    //itemList.length-1 =2 ，然后又是小于，所以currentId 在1的时候，就没有继续展示了
                    currentId < itemList.length - 1 &&
                    <Button type="primary" onClick={() => handleNext()}>
                        下一步
                    </Button>
                }
                &emsp;
                {
                    currentId > 0 &&
                    <Button type="primary" onClick={() => handlePrev()}>
                        上一步
                    </Button>
                }

            </div>
        </div>
    )
}
