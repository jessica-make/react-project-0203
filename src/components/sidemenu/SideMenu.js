import React, { useState, useEffect } from 'react'

import {
  UserOutlined,
  AppstoreOutlined,
  BankOutlined,
  EditOutlined,
  BarsOutlined
} from '@ant-design/icons';

import axios from 'axios';

import pubsub from 'pubsub-js'

import { Layout, Menu } from 'antd';

import './SideMenu.css'

import { withRouter } from 'react-router-dom'

import {connect} from 'react-redux'


const { Sider } = Layout;

const iconObj = {
  "/home": <BankOutlined />,
  "/user-manage": <UserOutlined />,
  "/user-manage/list": <UserOutlined />,
  "/right-manage": <AppstoreOutlined />,
  "/right-manage/role/list": <AppstoreOutlined />,
  "/right-manage/right/list": <AppstoreOutlined />,
  "/news-manage": <EditOutlined />,
  "/news-manage/add": <EditOutlined />,
  "/news-manage/draft": <EditOutlined />,
  "/news-manage/category": <BarsOutlined />,
}

function SideMenu(props) {
  const [filterMenu, setFilterMenu] = useState([])
  const [rights] = useState(JSON.parse(localStorage.getItem("Authorization")).role.rights)

  const [update, setUpdate] = useState(false)

  useEffect(() => {
    axios.get("/rights?_embed=children").then(res => {
      //react 属性需要全部小写，所以这里rightId 改为 rightid
      const arr = JSON.parse(JSON.stringify(res.data).replace(/rightId/g, 'rightid'));
      var newArr = arr.filter(k => recursionFilterMenu(k))
      setFilterMenu(newArr)
    })

    //递归过滤掉非一级路由，展示侧边栏
    function recursionFilterMenu(obj) {
      if (obj.children?.length > 0) {
        var newChildrenArr = obj.children.filter(k => recursionFilterMenu(k))
        obj.children = [...newChildrenArr]
      } else {
        //children属性为空，赋值为undefined,不要使用delete 会影响性能
        obj.children = undefined
      }

      //这不是js的语法，react可以像访问数组一样，访问对象的属性下标，得到组件value
      //obj.pagepermisson===x 表示这个可以展示，但是没有涉及到用户的权限
      obj.icon = iconObj[obj.key]
      return obj.hasOwnProperty("pagepermisson")
        && obj.pagepermisson === 1 && rights.includes(obj.key)
    }

    //监听权限列表 PermissionList.js 表格的 item.pagepermisson 是否改变
    pubsub.subscribe("isUpdate", (_, update) => {
      setUpdate(update)
    })

  }, [rights,update])


  //选择的路径
  const selectKeys = [props.location.pathname]
  const openKeys = ["/" + props.location.pathname.split("/")[1]]

  const onClick = (e) => {
    props.history.push(e.key)
  };

  return (
    <Sider trigger={null} collapsible collapsed={props.isCollapsed}>
      {/*flexDirection:'column'  flex 垂直布局
         设置这样布局，主要是让 React web管理系统 保持固定在顶部
      */}
      <div style={{ display: 'flex', height: '100%', flexDirection: 'column' }}>
        <div className="logo">React web管理系统</div>

        {/* 只有1个Menu，所以为1,会沾满全部，然后超出自动向下滚动 */}
        <div style={{ flex: 1, "overflow": "auto" }}>
          <Menu
            theme="dark"
            mode='inline'

            selectedKeys={selectKeys}
            defaultOpenKeys={openKeys}
            onClick={onClick}
            items={filterMenu}
          />
        </div>
      </div>
    </Sider>
  )
}

//获取折叠的初始属性
const mapStateToProps=({CollapsedReducer:{isCollapsed}})=>{
  return {
    isCollapsed
  }
}

export default connect(mapStateToProps)(withRouter(SideMenu))