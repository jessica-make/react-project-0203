import React, { useEffect, useState } from 'react'

import { DeleteOutlined, CloudUploadOutlined, StarOutlined,ExclamationCircleFilled } from '@ant-design/icons';

import { Table, Tag, Button, Modal,notification } from 'antd'

import axios from 'axios'

/**
 * 审核列表
 * @returns 
 */
export default function AuditList(props) {

  const { username } = JSON.parse(localStorage.getItem("Authorization"))

  const [dataSource, setDataSource] = useState([])

  useEffect(() => {
    //_ne=0 不等于0
    //查询新闻是当前用户的，不在草稿箱的，未发布或者待发布的
    axios.get(`/news?author=${username}&auditState_ne=0&publishState_lte=1&_expand=category`).then(res => {
      setDataSource(res.data)
    })
  }, [username])



  const columns = [
    {
      title: '新闻标题',
      dataIndex: 'title',
      render(title, item) {
        return <a href={`/news-manage/preview/${item.id}`}>{title}</a>
      },
    },
    {
      title: '作者',
      dataIndex: 'author',
    },
    {
      title: '新闻分类',
      dataIndex: 'category',
      render(category) {
        return <div>{category.value}</div>
      },
    },
    {
      title: '审核状态',
      dataIndex: 'auditState',
      render(auditState) {
        const colorList = ["", "orange", "green", "red"]
        const auditList = ["草稿箱", "审核中", "已通过", "未通过"]
        return <Tag color={colorList[auditState]}>{auditList[auditState]}</Tag>
      },
    },
    {
      title: '操作',
      render(item) {
        return <div>

          {
            item.auditState === 1 && <Button type='primary' onClick={() => handleRevert(item)}
              icon={<DeleteOutlined />}>撤销</Button>
          }


          {
            item.auditState === 2 && <Button type='primary' onClick={()=>handlePublish(item)}
              icon={<CloudUploadOutlined />}>发布</Button>
          }

          {
            item.auditState === 3 && <Button type='primary' onClick={() => handleUpdate(item)}
              icon={<StarOutlined />}>更新</Button>
          }


        </div>
      },
    },
  ]

  //撤销
  const handleRevert = (item) => {
    const { confirm } = Modal;
    confirm({
      title: `您确定要撤销${item.title}这条新闻吗?`,
      icon: <ExclamationCircleFilled />,
      onOk() {

        //假更新
        setDataSource(dataSource.filter(k => k.id !== item.id))

        //把这条新闻放到草稿箱里面
        axios.patch(`/news/${item.id}`, {
          auditState: 0
        })
      },
      onCancel() { },
    });


  }

  //更新 跳转到编辑页面
  const handleUpdate = (item) => {
    props.history.push(`/news-manage/update/${item.id}`)
  }

  //发布新闻
  const handlePublish=(item)=>{
    axios.patch(`/news/${item.id}`,{
      publishState:2,
      publishTime:Date.now()
     }).then(res=>
      setTimeout(() => {
        props.history.push('/publish-manage/published')
      }, 500),
      notification.info({
        message: "通知",
        description:`您可以到[发布管理/已发布]中查看您的新闻`,
        placement:"bottomRight"
      })
     )

  }

  return (
    <Table dataSource={dataSource} columns={columns}
      pagination={{ pageSize: 5 }}
      rowKey={item => item.id}
    />
  )
}
