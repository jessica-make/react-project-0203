import React, { useEffect, useState } from 'react'

import { Table, Button, notification } from 'antd'

import axios from 'axios'

/**
 * 对新闻进行审核
 * 
 * 要求: 只有同一区域的才可以审核，区域管理员可以审核自己和本区域的数据
 * 超级管理员可以审核全部的数据
 * @returns 
 */
export default function Audit() {
  const [dataSource, setDataSource] = useState([])

  const { roleId, region, username } = JSON.parse(localStorage.getItem("Authorization"))

  useEffect(() => {
    const roleObj = {
      "1": "superadmin",
      "2": "admin",
      "3": "editor"
    }

    //auditState 
    //0 草稿箱 1审核中 2已通过 3拒绝
    axios.get(`/news?auditState=1&_expand=category`).then(res => {
      var list = res.data

      //返回列表数据前做一次过滤，如果是超级管理员，返回全部数据
      //否则需要做一次过滤
      setDataSource(roleObj[roleId] === "superadmin" ? list : [
        //先拿到自己的数据
        ...list.filter(item => item.author === username),
        //再拿到和自己一个区域的数据，[]会做拼接
        ...list.filter(item => item.region === region &&
          roleObj[item.roleId] === "editor"
        )
      ]
      )
    })
  }, [roleId, region, username])


  const columns = [
    {
      title: '新闻标题',
      dataIndex: 'title',
      render(title, item) {
        return <a href={`/news-manage/preview/${item.id}`}>{title}</a>
      },
    },
    {
      title: '作者',
      dataIndex: 'author',
    },
    {
      title: '新闻分类',
      dataIndex: 'category',
      render(category) {
        return <div>{category.value}</div>
      },
    },
    {
      title: '操作',
      render(item) {
        return <div>
          <Button type='primary' onClick={() => handleAudit(item, 1)}>通过</Button> &emsp;
          <Button danger onClick={() => handleAudit(item, 0)}>驳回</Button>
        </div>
      },
    },
  ]

  const handleAudit = (item, flag) => {
    setDataSource(dataSource.filter(k => k.id !== item.id))

    if(flag ===1){
       //const auditList = ["草稿箱", "审核中", "已通过", "未通过"]
      //const publishList = ["未发布", "待发布", "已上线", "已下线"]

      //通过的新闻，审核状态 为已通过，发布状态为待发布
      axios.patch(`/news/${item.id}`,{
        auditState:2,
        publishState:1
      }).then(res =>
        notification.info({
          message: "通知",
          description: `您可以到[审核管理/审核列表]中查看您的新闻`,
          placement: "bottomRight"
        })
      )
    }else{
      //const auditList = ["草稿箱", "审核中", "已通过", "未通过"]
      //const publishList = ["未发布", "待发布", "已上线", "已下线"]

      //驳回的新闻，审核状态 为未通过，发布状态为未发布
      axios.patch(`/news/${item.id}`,{
        auditState:3,
        publishState:0
      }).then(res =>
        notification.info({
          message: "通知",
          description: `您可以到[审核管理/审核列表]中查看您的新闻`,
          placement: "bottomRight"
        })
      )
    }


  }

  return (
    <div>
      <Table dataSource={dataSource} columns={columns}
        pagination={{ pageSize: 5 }}
        rowKey={item => item.id}
      />
    </div>
  )
}
