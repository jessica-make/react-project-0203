import React, { useState } from 'react'
import { Form, Input, Select } from 'antd';

import pubsub from 'pubsub-js';

export default function UserForm(props) {
    const { form, regionList, roleList, modelTitle, roleName, region } = props

    const [isDisabled, setIsDisabled] = useState(false)

    pubsub.subscribe("shouldIsDisabled", (_, disabled) => {
        setIsDisabled(disabled)
    })

    const checkRegionDisabled=()=>{
          if(modelTitle==='修改用户'){
             if(roleName ==='超级管理员'){
                return false
             }else{
                return true
             }
          }else{
            if(roleName ==='超级管理员'){
                return false
             }else{
                //新增用户 往对象中，追加一个属性，使得当前区域的不禁用
                regionList.map(item=>{
                     if(item.value===region){
                        item.disabled=false
                     }else{
                        item.disabled=true
                     }
                     return item
                  }
                )
             }

          }
    }

    const checkRoleDisabled=()=>{
        if(modelTitle==='修改用户'){
            if(roleName ==='超级管理员'){
                return false
             }else{
                return true
             }
        }else{
            if(roleName ==='超级管理员'){
                return false
             }else{
               roleList.map(item=>{
                //当前用户不是超级管理员， 所以不能选择超级管理员作为新增角色
                  if(item.roleName==='超级管理员'){
                    item.disabled=true
                  }else{
                    item.disabled=false
                  }
                  return item
               })
         
             }
        }

    }

    return (
        <div>
            <Form
                form={form}
                layout="vertical"
                name="form_in_modal"
                initialValues={{
                    modifier: 'public',
                }}
            >
                <Form.Item
                    name="username"
                    label="用户名"
                    rules={[
                        {
                            required: true,
                            message: '用户名不能为空!',
                        },
                    ]}
                >
                    <Input type="TextArea" placeholder="请输入您的用户名" />
                </Form.Item>

                <Form.Item
                    name="password"
                    label="密码"
                    rules={[
                        {
                            required: true,
                            message: '密码不能为空!',
                        },
                    ]}
                >
                    <Input type="password" placeholder="请输入您的密码" />
                </Form.Item>

                <Form.Item
                    name="region"
                    label="区域"
                    rules={
                        //超级管理员不用填区域，为了避免报错，判断是不是超级管理员
                        //如果是,不用做规则校验  这里还可以采用三目 或者 &&
                        [
                            {
                                required: !isDisabled,
                                message: '区域不能为空!',
                            },
                        ]
                    }>
                    <Select allowClear 
                        placeholder="请您选择一个区域"
                        //这个地方，函数需要立即执行，返回true或者false
                        disabled={checkRegionDisabled()}

                        options={
                            regionList
                        }
                    />

                </Form.Item>

                <Form.Item
                    name="roleId"
                    label="角色"
                    rules={[
                        {
                            required: true,
                            message: '角色不能为空!',
                        },
                    ]}>

                    <Select
                        allowClear
                        options={roleList}
                        fieldNames={{ label: 'roleName', value: 'id' }}
                        placeholder="请您选择一个角色"

                        disabled={
                            checkRoleDisabled()
                        }
                      />

                </Form.Item>

            </Form>
        </div>
    )
}


