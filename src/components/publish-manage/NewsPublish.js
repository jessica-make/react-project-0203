import React from 'react'

import { Table} from 'antd';

export default function NewsPublish(props) {

  const {dataSource,button}=props

  const columns = [
  {
    title: '新闻标题',
    dataIndex: 'title',
    render: (tit,item) => <a href={`/news-manage/preview/${item.id}`}>{tit}</a>
  },

  {
    title: '作者',
    dataIndex: 'author',
  },

  {
    title: '新闻分类',
    dataIndex: 'category',
    render: (category) => {
      return category ===undefined?"":category.label
    }
  },
  {
    title: '操作',
    render(item) {
      return <div>
        {
          button(item.id)
        }
      </div>
    },
  },
  ];

  return (
    <Table dataSource={dataSource} columns={columns}
      pagination={{ pageSize: 5 }}
      rowKey={item => item.id}
    />
  )
}
