import React from 'react'

import NewsPublish from './NewsPublish';

import usePublishUtil from '../../utils/usePublishUtil';
import { Button } from 'antd';

/**
 * 待发布
 * 
 * @returns 
 */
export default function Unpublished() {
  
  const {dataSource,handlePublish} = usePublishUtil(1)

  return (
    <NewsPublish dataSource={dataSource} 
    button={(id)=>
    <Button type='primary' onClick={()=>handlePublish(id)}>发布
    </Button>
  }
    />
  )
}
