import React from 'react'
import usePublishUtil from '../../utils/usePublishUtil'

import NewsPublish from './NewsPublish'

import { Button } from 'antd';

/**
 * 已下线
 * 
 * @returns 
 */
export default function Sunset() {

  const {dataSource,handleDelete}=usePublishUtil(3)

  return (
    <NewsPublish dataSource={dataSource}
     button={(id)=><Button type='primary' onClick={()=>handleDelete(id)}>删除</Button>}/>
  )
}
