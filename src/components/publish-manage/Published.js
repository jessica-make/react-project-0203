import React from 'react'

import {Button} from 'antd';

import NewsPublish from './NewsPublish';

import usePublishUtil from '../../utils/usePublishUtil';

/**
 * 已发布
 * 
 * @returns 
 */
export default function Published() {

  const {dataSource,handleSunset}=usePublishUtil(2)

  return (
       <NewsPublish dataSource={dataSource}
        button={(id)=><Button type='primary' onClick={()=>handleSunset(id)}>下线</Button>}
        />
  )
}
