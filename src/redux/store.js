import { legacy_createStore,combineReducers } from "redux";

import CollapsedReducer from "./reducers/CollapsedReducer";

import LoadingReducer from "./reducers/LoadingReducer";

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' 

const reducer=combineReducers({
    //这里可以放多个reducer
    CollapsedReducer,
    LoadingReducer
})

/**
 * 持久化的key 相当于redis的 key
 */
const persistConfig = {
    key: 'react-project-0203',
    storage,
    //不需要持久化的 reducer
    blacklist: ['LoadingReducer'] 
  }

const persistedReducer = persistReducer(persistConfig, reducer)

const store=legacy_createStore(persistedReducer)
let perSistStore = persistStore(store)

export { 
    store,
    perSistStore 
}