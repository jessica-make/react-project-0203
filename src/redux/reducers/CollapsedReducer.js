const CollapsedReducer = (prevState = {
    //初始状态 不折叠
    isCollapsed: false
}, action) => {
    let { type } = action

    switch (type) {
        case "change_collapsed":
            let newState = { ...prevState }
            newState.isCollapsed = !newState.isCollapsed
            return newState
        default:
            return prevState
    }

}

export default CollapsedReducer