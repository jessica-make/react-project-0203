const LoadingReducer = (prevState = {
    //初始状态 不加载loading
    isLoading: false
}, action) => {
    let { type,payload} = action

    switch (type) {
        case "change_Loading":
            let newState = { ...prevState }
            newState.isLoading = payload
            return newState
        default:
            return prevState
    }

}

export default LoadingReducer